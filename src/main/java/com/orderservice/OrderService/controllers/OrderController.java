package com.orderservice.OrderService.controllers;

import com.orderservice.OrderService.dots.OrderDTO;
import com.orderservice.OrderService.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    //localhost:8083/api/order/getOrdersByUserId/1
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id){
        return orderService.getOrdersByUserId(id);
    }

    @PostMapping("/createOrder")
    public boolean createOrder(@RequestBody OrderDTO order){
        return orderService.createOrder(order);
    }

    @GetMapping("/getAllOrders")
    public List<OrderDTO> getAllOrders(){
        return orderService.getAllOrders();
    }

    @PutMapping("/updateOrder/{id}")
    public boolean updateOrder(@RequestBody OrderDTO order, @PathVariable final Long id) {
        return orderService.updateOrder(order, id);
    }
}
