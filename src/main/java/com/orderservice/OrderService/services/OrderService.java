package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dots.OrderDTO;
import com.orderservice.OrderService.entities.OrderEntity;
import com.orderservice.OrderService.repositories.OrderRepository;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository repository;

    public List<OrderDTO> getOrdersByUserId(Long id){
        List<OrderDTO> orders = null;
        try {
            orders = repository.findOrdersByUserId(id.toString())
                    .stream()
                    .map(order -> new OrderDTO(
                            order.getId().toString(),
                            order.getOrderId(),
                            order.getUserId()
                            )).collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.warn(" Exception in getOrdersByUserId");
        }
        return orders;
    }

    public boolean createOrder(OrderDTO order){
        try{
            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setId(orderEntity.getId());
            orderEntity.setOrderId(order.getId());
            orderEntity.setUserId(order.getId());
            repository.save(orderEntity);
            return true;

        }catch(Exception ex){
            LOGGER.warn("Error in OrderService" +ex);
        }
        return false;
    }

    public List<OrderDTO>getAllOrders(){
        LOGGER.info("/============= ENTER INFO getOrderService in OrderService ===========/");
        List<OrderDTO>orders = null;
        try{
            orders = repository.findAll()
                    .stream()
                    .map(orderEntity ->new OrderDTO(
                            orderEntity.getId().toString(),
                            orderEntity.getOrderId(),
                            orderEntity.getUserId()
                    )).collect(Collectors.toList());

        }catch (Exception ex){
            LOGGER.warn("Error in OrderService" +ex);
        }
        return orders;

    }

    public boolean updateOrder(OrderDTO order, Long id) {
        try {
           OrderEntity orderEntity = new OrderEntity();
            orderEntity.setOrderId(order.getOrderId());
            orderEntity.setUserId(order.getUserId());
            orderEntity.setId(id);
            repository.save(orderEntity);
            return true;

        } catch (Exception ex) {
            LOGGER.warn("Error in OrderService" + ex);
        }
        return false;
    }
}
